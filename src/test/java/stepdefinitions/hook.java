package stepdefinitions;

import org.openqa.selenium.WebDriver;

import com.gsnip.commonutilities.DriverManager;
import com.gsnip.commonutilities.Screenshot;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class hook extends DriverManager {

	public WebDriver driver;

	public static Scenario scenario;

	@Before
	public void setupDriver() {

		DriverManager.initiateBrowser("chrome");
	}

	@After
	public void closeDriver(Scenario scenario) {

		if (scenario.isFailed()) {

			byte[] Screenshots = Screenshot.getScreeshot();
			scenario.attach(Screenshots, "image/png", scenario.getName());

		}

		DriverManager.closeDriver();

	}

}
