package stepdefinitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.gsnip.commonutilities.DriverManager;
import com.gsnip.page.functionality.Website_Launch_Functionality;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class GsnipUserCreationandCartFunctionallityStepDef {

	JavascriptExecutor js;
	
	@Given("User is on gsnip {string}")
	public void user_is_on_gsnip(String gsnip) {
	    
		Website_Launch_Functionality.websiteLaunch(gsnip);
	}

	@When("Click on profile link and Register")
	public void click_on_profile_link_and_register() throws InterruptedException {
		DriverManager.getWebDriver().findElement(By.className("ct-header-account")).click();
		
		DriverManager.getWebDriver().findElement(By.id("reg_username")).sendKeys("finaltest009");
		
		DriverManager.getWebDriver().findElement(By.id("reg_email")).sendKeys("testing009@gmail.com");
		
		DriverManager.getWebDriver().findElement(By.id("reg_password")).sendKeys("Daroka#123");
		
		 js = (JavascriptExecutor) DriverManager.getWebDriver();
		
		js. executeScript("window. scrollBy(0,500)");
		
		Thread.sleep(3000);
		
		DriverManager.getWebDriver().findElement(By.name("register")).click();
	}

	@When("Go to address and register a address")
	public void go_to_address_and_register_a_address() throws InterruptedException {
	   
		List<WebElement> NavList =  DriverManager.getWebDriver().findElements(By.xpath("//nav[@class='woocommerce-MyAccount-navigation']//li"));
		
		js.executeScript("window. scrollBy(0,300)");
		
		Thread.sleep(3000);
		//click on addresss
		NavList.get(3).click();
		Thread.sleep(3000);
		DriverManager.getWebDriver().findElement(By.className("edit")).click();

		DriverManager.getWebDriver().findElement(By.id("billing_first_name")).sendKeys("Nikhil");
		DriverManager.getWebDriver().findElement(By.id("billing_last_name")).sendKeys("Darokar");
		DriverManager.getWebDriver().findElement(By.id("billing_company")).sendKeys("Testing Tech");
		js.executeScript("window. scrollBy(0,500)");
		DriverManager.getWebDriver().findElement(By.id("billing_address_1")).sendKeys("ABC House");
		DriverManager.getWebDriver().findElement(By.id("billing_address_2")).sendKeys("Tech Naka");
		DriverManager.getWebDriver().findElement(By.id("billing_city")).sendKeys("Mumbai");
		
	    js.executeScript("window. scrollBy(0,300)");
		
		Thread.sleep(3000);
		
	}

	@When("Select Maharashtra by searching in state")
	public void select_maharashtra_by_searching_in_state() throws InterruptedException {
		DriverManager.getWebDriver().findElement(By.id("select2-billing_state-container")).click();
		DriverManager.getWebDriver().findElement(By.className("select2-search__field")).sendKeys("Maharashtra");
		DriverManager.getWebDriver().findElement(By.className("select2-search__field")).sendKeys(Keys.ENTER);
		DriverManager.getWebDriver().findElement(By.id("billing_postcode")).sendKeys("400059");
		Thread.sleep(3000);
		DriverManager.getWebDriver().findElement(By.name("billing_phone")).sendKeys("7878778778");
		js.executeScript("window. scrollBy(0,300)");
		
		Thread.sleep(3000);
		DriverManager.getWebDriver().findElement(By.name("save_address")).click();
		
		
	}

	@Then("Verify that the added address is added or not")
	public void verify_that_the_added_address_is_added_or_not() {
		String verifyAddress = DriverManager.getWebDriver().findElement(By.className("woocommerce-message")).getText();

		Assert.assertEquals(verifyAddress, "Address changed successfully.");
	}

	@Then("Go to home Select apple from secondary header")
	public void go_to_home_select_apple_from_secondary_header() {
		DriverManager.getWebDriver().findElement(By.className("site-logo-container")).click();
		
		List<WebElement> ProductList = DriverManager.getWebDriver().findElements(By.xpath("(//ul[@id='menu-product-menu'])//li"));
		
		ProductList.get(0).click();
	}

	@Then("Select sorting from low to high and verify that the displayed products are sorted or not and print a success message")
	public void select_sorting_from_low_to_high_and_verify_that_the_displayed_products_are_sorted_or_not_and_print_a_success_message() {
		WebElement orderBy = DriverManager.getWebDriver().findElement(By.xpath("//select[@name='orderby']"));
		
		Select s  = new Select(orderBy);	

		s.selectByValue("price");
		
		String Price1 = DriverManager.getWebDriver().findElement(By.xpath("(//bdi)[1]")).getText();
		System.out.println("Price 1 Is : " + Price1);
		Price1 = Price1.replaceAll("[^\\d]", "");
		
		Integer LowerPrice =  Integer.valueOf(Price1);
		
		String Price2 = DriverManager.getWebDriver().findElement(By.xpath("(//bdi)[2]")).getText();
		System.out.println("Price 2 Is : " + Price2);
		Price2 = Price2.replaceAll("[^\\d]", "");
		Integer HigherPrice =  Integer.valueOf(Price2);

		if(LowerPrice<HigherPrice) {
			System.out.println("Products Are successFully Sorted.");
			
		}
		else {
			System.out.println("Products Are Not successFully Sorted.");
		}
		
	}

	@Then("Click on first product")
	public void click_on_first_product() {
		List<WebElement> ProdList = DriverManager.getWebDriver().findElements(By.xpath("//ul[@class='products columns-3']//li"));
		ProdList.get(0).click();
	}

	@Then("In detail page  Fetch the title of the product select Color red and Storage 512GB")
	public void in_detail_page_fetch_the_title_of_the_product_select_color_red_and_storage_512gb() throws InterruptedException {
		String ProdName = DriverManager.getWebDriver().findElement(By.tagName("h1")).getText();
		
		System.out.println("The ProductName is : " + ProdName);

		js.executeScript("window. scrollBy(0,800)");
		
		Thread.sleep(3000);
		
		WebElement colorName =  DriverManager.getWebDriver().findElement(By.xpath("//select[@name='attribute_pa_color']"));
		
		Select colorN  = new Select(colorName);	

		colorN.selectByValue("red");
		
		WebElement Storage =  DriverManager.getWebDriver().findElement(By.xpath("//select[@name='attribute_pa_storage']"));
		
		Select Storagesel  = new Select(Storage);	

		Storagesel.selectByValue("512-gb");
	}

	@Then("Select quantity {int} and add to cart")
	public void select_quantity_and_add_to_cart(Integer int1) throws InterruptedException {
	    
		DriverManager.getWebDriver().findElement(By.className("ct-increase")).click();
		
		Thread.sleep(3000);
		
		DriverManager.getWebDriver().findElement(By.xpath("//button[text() = 'Add to cart']")).click();
		
		//Fetch the sub-total of the cart by going to cart 
		Thread.sleep(10000);

		DriverManager.getWebDriver().findElement(By.className("ct-header-cart")).click();
		Thread.sleep(20000);
	}

	@Then("Fetch the sub-total of the cart by going to cart")
	public void fetch_the_sub_total_of_the_cart_by_going_to_cart() {
		
	}

	@Then("Go to cart fetch the sub-total and print it as message �The updated cart price is�")
	public void go_to_cart_fetch_the_sub_total_and_print_it_as_message_the_updated_cart_price_is() {
		String SubTotalPrice = DriverManager.getWebDriver().findElement(By.xpath("//div[@class='ct-panel-inner']//p//bdi")).getText();	
		System.out.println("The updated cart price is : " + SubTotalPrice);
	}

	@Then("Click on proceed to checkout")
	public void click_on_proceed_to_checkout() throws InterruptedException {
	    
		DriverManager.getWebDriver().findElement(By.className("button wc-forward")).click();
		Thread.sleep(3000);
		js.executeScript("window. scrollBy(0,500)");
		Thread.sleep(3000);
		DriverManager.getWebDriver().findElement(By.className("wc-proceed-to-checkout")).click();
	}
	
	
}
