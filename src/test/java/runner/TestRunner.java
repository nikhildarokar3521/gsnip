package runner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.mail.MessagingException;

import org.junit.runner.RunWith;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import com.gsnip.commonutilities.DateTimeUtil;
import com.gsnip.commonutilities.EmailUtil;
import com.gsnip.commonutilities.SendEmail;

import io.cucumber.java.AfterAll;
import io.cucumber.junit.Cucumber;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@RunWith(Cucumber.class)

@CucumberOptions(features = "src/test/resources/Features/", glue = {
		"stepdefinitions", "apistepdefinitions" }, plugin = {
				"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:" }, dryRun = false, monochrome = true

)

public class TestRunner extends AbstractTestNGCucumberTests {

	@Override
	@DataProvider(parallel = false)
	public Object[][] scenarios() {
		return super.scenarios();
	}

	@AfterSuite
	public void after_all() throws IOException, InterruptedException {


		List<String> recipients = Arrays.asList("nikhildarokar352@gmail.com");
		String subject = "ProjectName Regression Suite Report - " + DateTimeUtil.generateDate();
		String body = "Please find the attached report for Today's Regression suite. Thank you for your attention to this report, and we look forward to any feedback or insights you may have.";
		String attachmentPath = "./Reports/GSNIP.html";

		try {
			EmailUtil.sendEmailWithAttachment(recipients, subject, body, attachmentPath);
		} catch (MessagingException e) {
			// Handle any exceptions here
			e.printStackTrace();
		}
	}
}
