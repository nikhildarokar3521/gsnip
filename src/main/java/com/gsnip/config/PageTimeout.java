package com.gsnip.config;

import java.time.Duration;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gsnip.commonutilities.DriverManager;

public class PageTimeout {

//WebDriver Dynamic Waits to handle the Synchronization issue	
	public static WebElement waitforWebElement(WebElement ele,int timeinSec) {
		
			
		WebDriverWait wait = new WebDriverWait(DriverManager.getWebDriver(), Duration.ofSeconds(timeinSec));		
		wait.until(ExpectedConditions.visibilityOf(ele));
		return ele;
		
	}
}
