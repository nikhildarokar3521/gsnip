package com.gsnip.commonutilities;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;


public class CollectionsUtility {
	
	
	
	public static ArrayList<String> listToArraylist(List<WebElement> ele){
		
		List<WebElement> allLinks = ele;
		
		ArrayList<String> li1 = new ArrayList<String>();		
		 
		 //Traversing through the list and printing its text along with link address
	 for(WebElement link:allLinks){
		 
		 li1.add(link.getAttribute("href"));		 
		 }
		
		return li1;		
	}

}
