package com.gsnip.commonutilities;

import javax.lang.model.element.Element;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.github.dockerjava.api.model.Driver;

public class JavaScriptExecutor {
	
	
//Scrolling option Utility 
//Note :- Enter negative y value to scroll up and positive y value to scroll down
//Enter negative x value to scroll left and positive x value to scroll right
		
		public static void scroll(int x,int y) {
			
			JavascriptExecutor jse = (JavascriptExecutor) DriverManager.getWebDriver();
			jse.executeScript("window.scrollBy("+ Integer.toString(x)+","+ Integer.toString(y)+")" );
			
		}
	
//Scroll until visibilty of element
		

		public static void scrollUntilElementVisible(WebElement element) {
			JavascriptExecutor jse = (JavascriptExecutor) DriverManager.getWebDriver();
			jse.executeScript("arguments[0].scrollIntoView();", element);
			
		}

//Click WebElement Using JS Click
		
		public static void clickUsingJS(WebElement element) {
			
			JavascriptExecutor executor = (JavascriptExecutor) DriverManager.getWebDriver();
			executor.executeScript("arguments[0].click();", element);
		}

//SendKeys Method Using JS
		
		public static void sendKeysUsingJS(WebElement element, String inputText) {
			JavascriptExecutor executor = (JavascriptExecutor) DriverManager.getWebDriver();
			executor.executeScript("arguments[0].value = arguments[1]", element, inputText);
		}
		


}
