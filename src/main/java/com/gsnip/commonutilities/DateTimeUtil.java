package com.gsnip.commonutilities;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeUtil {

	public static String generateDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyy_HHmmss");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }

    
}
