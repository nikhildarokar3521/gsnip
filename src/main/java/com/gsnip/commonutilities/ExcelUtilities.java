package com.gsnip.commonutilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtilities {
	
//Data Fetching Utility
	
//To get the Physical RowCount of Current Excel
	
		public static int getRowCount(String pathOfExcel,String sheetName) throws Throwable, IOException {
			
			FileInputStream fis = new FileInputStream(pathOfExcel);
			Sheet sh = WorkbookFactory.create(fis).getSheet(sheetName);
			 
			int rowNo =sh.getPhysicalNumberOfRows();		
			return rowNo;
			
		}
		
//TO fetch String Type of Data
		
		public static String getTestStringData(String pathOfExcel,String sheetName,int rowNum,int cellNum) throws EncryptedDocumentException, IOException {
		
			FileInputStream fis = new FileInputStream(pathOfExcel);			
			Sheet sh = WorkbookFactory.create(fis).getSheet(sheetName);

			String data = sh.getRow(rowNum).getCell(cellNum).getStringCellValue();
			return data;

			
		}
		
//To fetch Numeric values from excel
		
		public static String getTestNumericData(String pathOfExcel,String sheetName,int rowNum,int cellNum) throws EncryptedDocumentException, IOException {
			
			FileInputStream fis = new FileInputStream(pathOfExcel);			
			Sheet sh = WorkbookFactory.create(fis).getSheet(sheetName);

			double data = sh.getRow(rowNum).getCell(cellNum).getNumericCellValue();
			
			String data1=String.valueOf(data);
			
			return data1;
			
		}


//To fetch Boolean values from excel
		
		 public static String getTestBooleanData(String pathOfExcel,String sheetName,int rowNum,int cellNum) throws EncryptedDocumentException, IOException {
					
			 FileInputStream fis = new FileInputStream(pathOfExcel);			
			 Sheet sh = WorkbookFactory.create(fis).getSheet(sheetName);

			 boolean data = sh.getRow(rowNum).getCell(cellNum).getBooleanCellValue();
					
			 String data1=String.valueOf(data);
					
			 return data1;
					
				}
		 
		 

//To write Data in Excel
		 
		 public static void writeDatainExcel(String excelPath,String SheetName,int rowNum, int cellNum, String Result) throws IOException {
			 
			 File file =    new File(excelPath);
			 XSSFWorkbook wb = new XSSFWorkbook();			 
			 
			 XSSFSheet sh = wb.createSheet(SheetName);
			 
			 sh.createRow(rowNum).createCell(cellNum).setCellValue(Result);
			 
			 wb.close();
	
			 
		 }
		 

		 

		
}
