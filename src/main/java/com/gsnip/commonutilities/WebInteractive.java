package com.gsnip.commonutilities;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gsnip.commonutilities.DriverManager;
import com.gsnip.config.PageTimeout;
import com.gsnip.config.config;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class WebInteractive {

//------------------------------------------------------------------------------------------------------------------		
//DropDown Option Selection Utility	

	public static void selectFromDropDown(WebElement dropdown, String howto, String option) {

		Select select = new Select(dropdown);
		switch (howto) {
		case "Index":
			select.selectByIndex(Integer.parseInt(option));
			break;
		case "value":
			select.selectByValue(option);
			break;
		case "text":
			select.selectByVisibleText(option);
			break;
		default:
			System.out.println(
					"Please provide a valis selection in the feature file, valid selections are: text, value, index");
			break;
		}
	}

//Another Method  Dropdown selection

	public static void selectOption(WebElement dropdownElement, String option) {

		Select dropdown = new Select(dropdownElement);
		dropdownElement.click();
		WebElement desiredOption = dropdownElement.findElement(By.cssSelector("option[value='" + option + "']"));
		desiredOption.click();
	}

//Another Method  Dropdown selection

	public static void dynamicDropdown(WebElement dropdown, List<WebElement> options, String desired,
			String actualoption) {
		dropdown.sendKeys(desired);
		for (WebElement option : options) {
			if (option.getAttribute("innerText").equalsIgnoreCase(actualoption)) {
				option.click();
				break;
			}
		}
	}

//Another Method  Dropdown selection

	public static void selectOptionAfterWaiting(WebElement dropdownElement, String Option) throws InterruptedException {

		Select dropdown = new Select(dropdownElement);
		dropdownElement.click();
		Thread.sleep(3000);
		WebElement desiredOption = dropdownElement.findElement(By.cssSelector("option[value='" + Option + "']"));
		desiredOption.click();
	}

//Another Method  Dropdown selection

	public static void dynamicDropdownWithoutInput(List<WebElement> options, String actualoption)
			throws InterruptedException {
		for (WebElement option : options) {
			if (option.getAttribute("innerText").equalsIgnoreCase(actualoption)) {
				option.click();
				break;

			}
		}
	}

//------------------------------------------------------------------------------------------------------------------		
//WebElement Click Utility

	public static void clickeElement(WebElement ele, long waitTime) {

		WebDriverWait wait = new WebDriverWait(DriverManager.getWebDriver(), Duration.ofSeconds(waitTime));
		wait.until(ExpectedConditions.elementToBeClickable(ele));

		ele.click();
	}

//------------------------------------------------------------------------------------------------------------------		
//SendKeys Utility

	public static void sendkeys(WebElement ele, String testData) {

		PageTimeout.waitforWebElement(ele, 20);

		ele.click();
		ele.clear();
		ele.sendKeys(testData);
	}

//------------------------------------------------------------------------------------------------------------------			
//Alert PopUp Handling Utility	

	public static void acceptorcancelAlert(String acceptorcancel) {

		if (acceptorcancel.equalsIgnoreCase("ok") || acceptorcancel.equalsIgnoreCase("accept")) {

			DriverManager.getWebDriver().switchTo().alert().accept();
		} else {

			DriverManager.getWebDriver().switchTo().alert().dismiss();
		}

	}

//------------------------------------------------------------------------------------------------------------------	
//Upload file utility

	public static void uploadFile(String FilePath, WebElement ele) throws AWTException, InterruptedException {

		ele.click();
		Thread.sleep(2000);
		Robot rb = new Robot();
		StringSelection str = new StringSelection(FilePath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);

		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_V);

		rb.keyRelease(KeyEvent.VK_CONTROL);
		rb.keyRelease(KeyEvent.VK_V);

		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);

	}

//------------------------------------------------------------------------------------------------------------------		
//To Capture Current Url Status Code

	public static int captureStatusCode(String url) {

		RestAssured.baseURI = url;

		RequestSpecification http = RestAssured.given();

		Response res = http.get(url);

		int StatusCode = res.getStatusCode();

		return StatusCode;

	}

//------------------------------------------------------------------------------------------------------------------	

//To Capture Status code of All curls present on Component	

	public static void captureStatusOfCodeEachLinkOnComponent(List<WebElement> listOfUrlPresentOnComponent,
			String ExcelFilePath, String SheetName) throws IOException {

		ArrayList<String> li1 = CollectionsUtility.listToArraylist(listOfUrlPresentOnComponent);

		File file = new File(ExcelFilePath);
		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sh = wb.createSheet(SheetName);
		FileOutputStream fos = new FileOutputStream(file);

		for (int i = 0; i < li1.size(); i++)

		{

			try {

				DriverManager.getWebDriver().navigate().to(li1.get(i));
				;

				String currenturl = DriverManager.getWebDriver().getCurrentUrl();
				int res = WebInteractive.captureStatusCode(currenturl);

				String output = "HTTP Response for the URL : " + currenturl + " is : " + res;

				System.out.println(output);
				sh.createRow(i).createCell(0).setCellValue(output);

				wb.write(fos);

			}

			catch (Exception e) {

				String output = "The " + i + "th" + " Number link is not working on Component";

				System.out.println(output);

				sh.createRow(i).createCell(0).setCellValue(output);

				wb.write(fos);

			}
		}
	}

//------------------------------------------------------------------------------------------------------------------	
//To Capture StatusCode of Header Component

	public static void captureStatusCodeForHeader(String ExcelFilePath, String SheetName) throws IOException {
		List<WebElement> anchorTags = new ArrayList<>();
		List<WebElement> headerTags = DriverManager.getWebDriver().findElements(By.tagName("header"));

		anchorTags.addAll(getAnchorTagsFromElements(headerTags));

		File file = new File(ExcelFilePath);
		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sh200 = wb.createSheet(SheetName + " (200)");
		XSSFSheet shNon200 = wb.createSheet(SheetName + " (Otherthan200)");
		FileOutputStream fos = new FileOutputStream(file);

		Row headerRow200 = sh200.createRow(0);
		headerRow200.createCell(0).setCellValue("HyperlinkText");
		headerRow200.createCell(1).setCellValue("URL");
		headerRow200.createCell(2).setCellValue("Statuscode");
		headerRow200.createCell(3).setCellValue("Link No.");

		Row headerRowNon200 = shNon200.createRow(0);
		headerRowNon200.createCell(0).setCellValue("HyperlinkText");
		headerRowNon200.createCell(1).setCellValue("URL");
		headerRowNon200.createCell(2).setCellValue("Statuscode");
		headerRowNon200.createCell(3).setCellValue("Link No.");

		int rowIdx200 = 1;
		int rowIdxNon200 = 1;

		for (int i = 0; i < anchorTags.size(); i++) {
			String href = anchorTags.get(i).getAttribute("href");
			String status;
			String hyperLinkedText = anchorTags.get(i).getText();
			String dataHref = anchorTags.get(i).getAttribute("data-href");

			if (href != null && href.contains("https://")) {
				status = getLinkStatus(href);

				Row row;
				if (status.equals("200")) {
					row = sh200.createRow(rowIdx200);
					row.createCell(0).setCellValue(hyperLinkedText);
					row.createCell(1).setCellValue(href);
					row.createCell(2).setCellValue(status);
					row.createCell(3).setCellValue(i + 1);
					rowIdx200++;
				} else {
					row = shNon200.createRow(rowIdxNon200);
					row.createCell(0).setCellValue(hyperLinkedText);
					row.createCell(1).setCellValue(href);
					row.createCell(2).setCellValue(status);
					row.createCell(3).setCellValue(i + 1);
					rowIdxNon200++;
				}
			} else {
				status = getLinkStatus(config.gsnip + dataHref);

				Row row;
				if (status.equals("200")) {
					row = sh200.createRow(rowIdx200);
					row.createCell(0).setCellValue(hyperLinkedText);
					row.createCell(1).setCellValue(dataHref);
					row.createCell(2).setCellValue(status);
					row.createCell(3).setCellValue(i + 1);
					rowIdx200++;
				} else {
					row = shNon200.createRow(rowIdxNon200);
					row.createCell(0).setCellValue(hyperLinkedText);
					row.createCell(1).setCellValue(dataHref);
					row.createCell(2).setCellValue(status);
					row.createCell(3).setCellValue(i + 1);
					rowIdxNon200++;
				}
			}
		}

		wb.write(fos);
	}

	private static List<WebElement> getAnchorTagsFromElements(List<WebElement> elements) {
		List<WebElement> anchorTags = new ArrayList<>();

		for (WebElement element : elements) {
			List<WebElement> anchors = element.findElements(By.tagName("a"));
			anchorTags.addAll(anchors);
		}

		return anchorTags;
	}

	private static String getLinkStatus(String href) {
		try {
			URL url = new URL(href);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("HEAD");
			connection.connect();
			int responseCode = connection.getResponseCode();
			connection.disconnect();
			return Integer.toString(responseCode);
		} catch (Exception e) {
			return "Error: " + e.getMessage();
		}
	}

//------------------------------------------------------------------------------------------------------------------	
//Status Code Capturing For Footer Links		    

	public static void captureStatusCodeForFooter(String ExcelFilePath, String SheetName) throws IOException {
		List<WebElement> anchorTags = new ArrayList<>();
		List<WebElement> headerTags = DriverManager.getWebDriver().findElements(By.tagName("footer"));

		anchorTags.addAll(getAnchorTagsFromElements(headerTags));

		File file = new File(ExcelFilePath);
		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sh200 = wb.createSheet(SheetName + " (200)");
		XSSFSheet shNon200 = wb.createSheet(SheetName + " (Otherthan200)");
		FileOutputStream fos = new FileOutputStream(file);

		Row headerRow200 = sh200.createRow(0);
		headerRow200.createCell(0).setCellValue("HyperlinkText");
		headerRow200.createCell(1).setCellValue("URL");
		headerRow200.createCell(2).setCellValue("Statuscode");
		headerRow200.createCell(3).setCellValue("Link No.");

		Row headerRowNon200 = shNon200.createRow(0);
		headerRowNon200.createCell(0).setCellValue("HyperlinkText");
		headerRowNon200.createCell(1).setCellValue("URL");
		headerRowNon200.createCell(2).setCellValue("Statuscode");
		headerRowNon200.createCell(3).setCellValue("Link No.");

		int rowIdx200 = 1;
		int rowIdxNon200 = 1;

		for (int i = 0; i < anchorTags.size(); i++) {
			String href = anchorTags.get(i).getAttribute("href");
			String status;
			String hyperLinkedText = anchorTags.get(i).getText();
			String dataHref = anchorTags.get(i).getAttribute("data-href");

			if (href != null && href.contains("https://")) {
				status = getLinkStatus(href);

				Row row;
				if (status.equals("200")) {
					row = sh200.createRow(rowIdx200);
					row.createCell(0).setCellValue(hyperLinkedText);
					row.createCell(1).setCellValue(href);
					row.createCell(2).setCellValue(status);
					row.createCell(3).setCellValue(i + 1);
					rowIdx200++;
				} else {
					row = shNon200.createRow(rowIdxNon200);
					row.createCell(0).setCellValue(hyperLinkedText);
					row.createCell(1).setCellValue(href);
					row.createCell(2).setCellValue(status);
					row.createCell(3).setCellValue(i + 1);
					rowIdxNon200++;
				}
			} else {
				status = getLinkStatus(config.gsnip + dataHref);

				Row row;
				if (status.equals("200")) {
					row = sh200.createRow(rowIdx200);
					row.createCell(0).setCellValue(hyperLinkedText);
					row.createCell(1).setCellValue(dataHref);
					row.createCell(2).setCellValue(status);
					row.createCell(3).setCellValue(i + 1);
					rowIdx200++;
				} else {
					row = shNon200.createRow(rowIdxNon200);
					row.createCell(0).setCellValue(hyperLinkedText);
					row.createCell(1).setCellValue(dataHref);
					row.createCell(2).setCellValue(status);
					row.createCell(3).setCellValue(i + 1);
					rowIdxNon200++;
				}
			}
		}

		wb.write(fos);
	}

//------------------------------------------------------------------------------------------------------------------		
//Actions Class Methods		

	public static void moveToElementusingActions(WebElement element) {
		Actions act = new Actions(DriverManager.getWebDriver());
		act.moveToElement(element).perform();
	}


//------------------------------------------------------------------------------------------------------------------		
//Conversion of One Date Format To Another Format
	
	public static String convertDateFormat(String inputDate, String outputFormat) {
		try {
			DateTimeFormatter inputFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
			ZonedDateTime zonedDateTime = ZonedDateTime.parse(inputDate, inputFormatter);

			DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern(outputFormat);
			LocalDateTime localDateTime = zonedDateTime.toLocalDateTime();

			return localDateTime.format(outputFormatter);
		} catch (Exception e) {
			System.err.println("Error converting date: " + e.getMessage());
			return null;
		}
	}

//-----------------------------------------------------------------------------------------------------------------
//Fetch data From Console - dataMapping data
	
	public String getDataThroughDataMapping() {
		Object dataMapping = ((JavascriptExecutor) DriverManager.getWebDriver())
				.executeScript("return window.dataMapping;");

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		// Convert the object to a pretty string
		String prettyString = gson.toJson(dataMapping);
		return prettyString;
	}
	
//Fetch Particular Json from Complex Json on Key Basis
	
	public static JSONObject findParticularJsonWithKey(String jsonString, String targetPolicyNo, String keyValue)
			throws JSONException {
		JSONArray jsonArray = new JSONArray(jsonString);
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject getJson = jsonArray.getJSONObject(i);
			String ParticularkeyValue = getJson.getString(keyValue);
			if (ParticularkeyValue.equals(targetPolicyNo)) {
				return getJson;
			}
		}
		return null;
	}
//-----------------------------------------------------------------------------------------------------------------
//Conversion of Excel File Data To Json
	
	  private static String convertExcelToJson(String excelFilePath) {
	        List<Map<String, String>> productList = readExcel(excelFilePath);
	        return convertToJSON(productList);
	    }

	    private static List<Map<String, String>> readExcel(String filePath) {
	        List<Map<String, String>> productList = new ArrayList<>();

	        try (FileInputStream excelFile = new FileInputStream(new File(filePath));
	             Workbook workbook = new XSSFWorkbook(excelFile)) {
	            Sheet sheet = workbook.getSheetAt(0);

	            Row headerRow = sheet.getRow(0);
	            int colCount = headerRow.getLastCellNum();

	            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
	                Row row = sheet.getRow(i);
	                Map<String, String> product = new HashMap<>();

	                for (int j = 0; j < colCount; j++) {
	                    Cell cell = row.getCell(j);
	                    String headerValue = headerRow.getCell(j).getStringCellValue();
	                    String cellValue = getCellValueAsString(cell);

	                    product.put(headerValue, cellValue);
	                }

	                productList.add(product);
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        }

	        return productList;
	    }

	    private static String getCellValueAsString(Cell cell) {
	        if (cell == null) {
	            return "";
	        } else if (cell.getCellType() == CellType.NUMERIC) {
	            if (DateUtil.isCellDateFormatted(cell)) {
	                return cell.getDateCellValue().toString();
	            } else {
	                return String.valueOf(cell.getNumericCellValue());
	            }
	        } else if (cell.getCellType() == CellType.BOOLEAN) {
	            return String.valueOf(cell.getBooleanCellValue());
	        } else {
	            return cell.getStringCellValue();
	        }
	    }

	    private static String convertToJSON(List<Map<String, String>> productList) {
	        ObjectMapper objectMapper = new ObjectMapper();
	        try {
	            return objectMapper.writeValueAsString(productList);
	        } catch (IOException e) {
	            e.printStackTrace();
	            return null;
	        }
	    }
//---------------------------------------------------------------------------------------------------	
}
