package com.gsnip.pagefactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.gsnip.commonutilities.DriverManager;

public class GsnipXpaths {

	@FindBy(id="shipping_first_name") public WebElement FirstName;
	
	public GsnipXpaths() {
		
		PageFactory.initElements(DriverManager.getWebDriver(), this);

	}

	
}
